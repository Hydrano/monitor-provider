﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace WebChecker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CheckCrowdLoginController : ControllerBase
    {
        private readonly ILogger<CheckCrowdLoginController> _logger;

        public CheckCrowdLoginController(ILogger<CheckCrowdLoginController> logger)
        {
            _logger = logger;
        }

        [HttpGet(Name = "GetCheckCrowdLogin")]
        public string Get()
        {
            string html = string.Empty;
            string url = @"https://appme.miele.com/dvcs/-/readiness?token=DaKWbMkEdZNmEipsQN_b";

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.AutomaticDecompression = DecompressionMethods.GZip;
            request.Timeout = 48000;

            request.CookieContainer = new CookieContainer();

            Uri uri = new Uri("https://appme.miele.com");
            Cookie mRHSession_cookie = new Cookie("MRHSession", "b99d683bf96126bed5d83459a5b3d932", "/");
            Cookie lastMRH_Session_cookie = new Cookie("LastMRH_Session", "LastMRH_Session=a5b3d932", "/");


            request.CookieContainer.Add(uri, mRHSession_cookie);
            request.CookieContainer.Add(uri, lastMRH_Session_cookie);


            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                html = reader.ReadToEnd();
            }

            Console.WriteLine(html);

            return html;
        }
    }
}
